package com.npe.socialbots.consumer;

import com.npe.dto.kafka.MessageWrapper;
import com.npe.dto.kafka.in.NotificationDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class MessageProcessConsumer {

    private final MessageService messageService;

    public final static String MESSAGE_START = "SEND_CONTACT_NOTIFICATION";

    @KafkaListener(topics = "${spring.kafka.topics.proxy}")
    public void startMessageProcess(String message) {
        messageService.correlateMessage(message, MESSAGE_START);
    }

}
