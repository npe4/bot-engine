package com.npe.socialbots.consumer;

import org.camunda.bpm.engine.runtime.MessageCorrelationResult;

public interface MessageService {

    MessageCorrelationResult correlateMessage(String message, String messageName);
}
