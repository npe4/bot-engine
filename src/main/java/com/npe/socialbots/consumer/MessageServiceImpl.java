package com.npe.socialbots.consumer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.npe.dto.kafka.MessageWrapper;
import com.npe.dto.kafka.in.NotificationDto;
import com.npe.socialbots.utils.ConversionUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.MismatchingMessageCorrelationException;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.rest.dto.message.MessageCorrelationResultDto;
import org.camunda.bpm.engine.runtime.MessageCorrelationBuilder;
import org.camunda.bpm.engine.runtime.MessageCorrelationResult;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.npe.socialbots.consumer.MessageProcessConsumer.MESSAGE_START;

@Service
@RequiredArgsConstructor
@Slf4j
public class MessageServiceImpl implements MessageService {


    private final RuntimeService runtimeService;
    private final ObjectMapper objectMapper;

    public MessageCorrelationResult correlateMessage(String message, String messageName) {
        try {
            log.info("Consuming message {}", messageName);

            MessageWrapper<?> messageWrapper =
                    ConversionUtils.jsonToJava(message, MessageWrapper.class);
            MessageCorrelationBuilder messageCorrelationBuilder = runtimeService.createMessageCorrelation(messageName);

            MessageCorrelationResult messageResult = messageCorrelationBuilder
                    .processInstanceBusinessKey(messageWrapper.getCorrelationId())
                    .setVariable(MESSAGE_START, new ObjectMapper().writeValueAsString(message))
                    .correlateWithResult();

            String messageResultJson = new ObjectMapper()
                    .writeValueAsString(MessageCorrelationResultDto.fromMessageCorrelationResult(messageResult));

            log.info("Correlation successful. Process Instance Id: {}", messageResultJson);
            log.info("Correlation key used: {}", messageWrapper.getCorrelationId());

            return messageResult;
        } catch (MismatchingMessageCorrelationException e) {
            log.error("Issue when correlating the message: {}", e.getMessage());
        } catch (Exception e) {
            log.error("Unknown issue occurred", e);
        }
        return null;
    }
}
