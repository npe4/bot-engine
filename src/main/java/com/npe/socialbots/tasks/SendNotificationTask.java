package com.npe.socialbots.tasks;

import com.npe.dto.kafka.MessageWrapper;
import com.npe.dto.kafka.in.NotificationDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import static com.npe.socialbots.consumer.MessageProcessConsumer.MESSAGE_START;


@Slf4j
@RequiredArgsConstructor
@Component
public class SendNotificationTask implements JavaDelegate {

    private final KafkaTemplate<String, String> kafkaTemplate;

    private final NewTopic botsTopic;
    
    public void execute(DelegateExecution execution) throws Exception {
        log.info("Send notification");
        kafkaTemplate.send(botsTopic.name(), String.valueOf(execution.getVariable(MESSAGE_START)));
    }

}
